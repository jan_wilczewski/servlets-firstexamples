package Filter;

import javax.servlet.*;
import java.io.IOException;
import java.util.logging.Filter;
import java.util.logging.LogRecord;

public class PageFilter implements javax.servlet.Filter{

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        long start = System.currentTimeMillis();
        System.out.println("Start: " + start);

        // niech się przetwarza dalej
        filterChain.doFilter(servletRequest, servletResponse);

        long end = System.currentTimeMillis();
        System.out.println("End: " + end);
        System.out.println("Time: " + (end - start));
    }

    @Override
    public void destroy() {

    }
}
