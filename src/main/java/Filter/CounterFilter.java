package Filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class CounterFilter implements Filter {

    private long count = 0;

    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        chain.doFilter(req, resp);

        ++count;
        System.out.println("Ilość wejść: " + count);
        PrintWriter writer = resp.getWriter();
        writer.write("Welcome from CounterFilter");

        HttpServletResponse r = (HttpServletResponse) resp;
        r.addHeader("Counter", String.valueOf(count));
    }

    public void init(FilterConfig config) throws ServletException {
    }
}
