import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet(
        name = "HelloServlet",
        urlPatterns = {"/hello", "/Hello"},
        initParams = {
                @WebInitParam(name = "who", value = "World"),
                @WebInitParam(name = "times", value = "5")
        },
        loadOnStartup = 1
)
public class HelloServlet extends HttpServlet {

    private String who;
    private int times;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        this.who = getInitParameter("who");
        this.times = Integer.parseInt(getInitParameter("times"));
        System.out.println("Pobieram init parametr 'who'= " + who);
        System.out.println("Pobieram init parametr 'times'= " + times);

    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        out.println("bbbbb!");
//        out.println("who = " + who);
//        out.println("times = " + times);

        for (int i = 0; i < times ; i++) {
            out.println("who = " + who);
            //out.println("times = " + times);
        }
    }

    @Override
    public void destroy() {
        super.destroy();
        System.out.println("To już jest koniec.");
    }
}
